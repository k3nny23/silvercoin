package com.kennycode.silvercoin.model;

import java.time.LocalDate;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {

	/**
	 * Explanations
	 * 
	 * With JavaFX it's common to use Properties for all fields of a model class. A
	 * Property allows us, for example, to automatically be notified when the
	 * lastName or any other variable is changed. This helps us keep the view in
	 * sync with the data. To learn more about Properties read Using JavaFX
	 * Properties and Binding.
	 * 
	 * LocalDate, the type we're using for birthday, is part of the new Date and
	 * Time API for JDK 8.
	 */

	private final StringProperty firstName;
	private final StringProperty lastName;
	private final StringProperty street;
	private final IntegerProperty postalCode;
	private final StringProperty city;
	private final ObjectProperty<LocalDate> birthday;

	/**
	 * Default constructor;
	 */
	public Person() {
		this(null, null);
	}

	public Person(String firstName, String lastName) {
		this.firstName = new SimpleStringProperty(firstName);
		this.lastName = new SimpleStringProperty(lastName);

		// some initial dummy data, just for convenient testing

		this.street = new SimpleStringProperty("some street");
		this.postalCode = new SimpleIntegerProperty(1234);
		this.city = new SimpleStringProperty("some city");
		this.birthday = new SimpleObjectProperty<LocalDate>(LocalDate.of(1999, 8, 25));

	}

	public StringProperty getFirstName() {
		return firstName;
	}

	public StringProperty getLastName() {
		return lastName;
	}

	public StringProperty getStreet() {
		return street;
	}

	public IntegerProperty getPostalCode() {
		return postalCode;
	}

	public StringProperty getCity() {
		return city;
	}

	public ObjectProperty<LocalDate> getBirthday() {
		return birthday;
	}

}
